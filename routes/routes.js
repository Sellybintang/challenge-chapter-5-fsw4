const Routes = require("express");
const routes = Routes()
const {getProducts, createProduct, updateProduct, getProductsId} = require ("../controllers/carsController")
const uploader = require('../middleware/uploader')
const adminCarController = require("../controllers/admin/adminCarController")


// Views
routes.get('/',adminCarController.getProducts)
routes.get('/addNewCar',adminCarController.addNewCar)

// API buat nampilin produk cars
routes.get('/api', getProducts)
// API buat create cars
routes.post('/api',uploader.single('Foto'),createProduct)
routes.get('/api/:id', getProductsId)
routes.put('/api/:id',uploader.single('Foto'),updateProduct)
// routes.delete('/api',deleteProduct)





module.exports=routes