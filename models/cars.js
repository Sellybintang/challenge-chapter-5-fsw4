'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Cars.init({
    Nama: DataTypes.STRING,
    Sewa_Per_Hari: DataTypes.STRING,
    Ukuran: DataTypes.STRING,
    Foto: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Cars',
  });
  return Cars;
};