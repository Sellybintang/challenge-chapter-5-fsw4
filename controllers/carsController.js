const {Op, where} = require ("Sequelize");
const {Cars} = require ("../models")
const cloudinary =require ("../lib/cloudinary")


// GET /cars
async function getProducts(req, res) {
    const items = await Cars.findAll()
    res.json ({
        "massage": "Sukses",
        "data": items
    });
};



// create
async function createProduct(req, res){
    const fileBase64 = req.file.buffer.toString("base64");
    const file = `data:${req.file.mimetype};base64,${fileBase64}`;

    const uploaded = await cloudinary.uploader.upload(file, (err) => {
      if (!!err) {
        console.log(err);
        return res.status(400).json({
          message: "Gagal mengupload foto!",
        });
      }
    });

    const{Nama,Sewa_Per_Hari,Ukuran}=req.body
    Cars.create({
    Nama,Sewa_Per_Hari,Ukuran,Foto:uploaded.url
})

    res.json({
        "message":"Data Berhasil Ditambahkan" 
    })

}

// Update
async function updateProduct(req, res){
    const fileBase64 = req.file.buffer.toString("base64");
    const file = `data:${req.file.mimetype};base64,${fileBase64}`;

    const uploaded = await cloudinary.uploader.upload(file, (err) => {
      if (!!err) {
        console.log(err);
        return res.status(400).json({
          message: "Gagal mengupload foto!",
        });
      }
    });

    const{Nama,Sewa_Per_Hari,Ukuran}=req.body
    Cars.update({
    Nama,Sewa_Per_Hari,Ukuran,id,Foto:uploaded.url},{ 
      where:{id:req.params.id}
    })

    res.json({
        "message":"Data Berhasil Ditambahkan",
        
    })

}

// Get by id
async function getProductsId(req, res) {
  const{Nama,Sewa_Per_Hari,Ukuran}=req.body
  const id=req.params.id
  const items = await Cars.findByPk(id)

  if ({Nama,Sewa_Per_Hari,Ukuran}==null)
  return res.status(400).json({
    message: "data ${id} tidak ditemukan!",
  })

  res.json ({
      "massage": "Sukses",
      "data": items
  })
};

// Delete



module.exports={
    getProducts,
    createProduct,
    updateProduct,
    getProductsId,
    

}

